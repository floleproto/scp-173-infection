﻿using System.Timers;
using Smod2;
using Smod2.API;
using Smod2.EventHandlers;
using Smod2.Events;
using Smod2.EventSystem.Events;

namespace SCP_173_Infection
{
    internal class PluginEvent : IEventHandlerSetRole, IEventHandlerPlayerDie, IEventHandlerPlayerTriggerTesla, IEventHandlerTeamRespawn
    {
        private Peanut peanut;

        public PluginEvent(Peanut peanut)
        {
            this.peanut = peanut;
        }

        public void OnPlayerDie(PlayerDeathEvent ev)
        {
            Vector pos = ev.Killer.GetPosition();
            Player v = ev.Player;
            Player k = ev.Killer;
            if(k.TeamRole.Team == Smod2.API.Team.SCP && v.TeamRole.Team == Smod2.API.Team.CLASSD)
            {
                ev.SpawnRagdoll = false;
                Timer t = new Timer();

                t.AutoReset = false;
                t.Interval = 2000;
                t.Enabled = true;
                t.Start();
                t.Elapsed += delegate
                {
                    v.ChangeRole(Role.SCP_173, true, true, true);
                    v.Teleport(pos, true);
                };

            }else if (k.TeamRole.Team == Smod2.API.Team.CLASSD && v.TeamRole.Team == Smod2.API.Team.SCP)
            {
                k.AddHealth(10);
                k.SetAmmo(AmmoType.DROPPED_5, k.GetAmmo(AmmoType.DROPPED_5) + 10);
            }
        }

        public void OnPlayerTriggerTesla(PlayerTriggerTeslaEvent ev)
        {
            if (ev.Player.TeamRole.Team == Smod2.API.Team.SCP && ConfigManager.Manager.Config.GetBoolValue("pean_173_tesla", false) == true)
                ev.Triggerable = true;
            else
                ev.Triggerable = false;

            if (ev.Player.TeamRole.Team == Smod2.API.Team.CLASSD && ConfigManager.Manager.Config.GetBoolValue("pean_classd_tesla", false) == true)
                ev.Triggerable = true;
            else
                ev.Triggerable = false;

            if (ev.Player.TeamRole.Team == Smod2.API.Team.CHAOS_INSURGENCY && ConfigManager.Manager.Config.GetBoolValue("pean_ic_tesla", false) == true)
                ev.Triggerable = true;
            else
                ev.Triggerable = false;
        }

        public void OnSetRole(PlayerSetRoleEvent ev)
        {
            ev.UsingDefaultItem = false;
            Player p = ev.Player;
            if (ev.TeamRole.Team == Smod2.API.Team.SCP && ev.Role != Role.SCP_173)
            {
                ev.Role = Role.SCP_173;

                p.SetGhostMode(true, true, true);
                p.SetHealth(ConfigManager.Manager.Config.GetIntValue("pean_173_hp", 2000));
            }
            if(ev.TeamRole.Team == Smod2.API.Team.CLASSD)
            {
               ev.Items.Add(ItemType.ZONE_MANAGER_KEYCARD);
               ev.Items.Add(ItemType.E11_STANDARD_RIFLE);
               p.SetGhostMode(false, false, false);
               p.SetHealth(ConfigManager.Manager.Config.GetIntValue("pean_classd_hp", 120));
               p.SetAmmo(AmmoType.DROPPED_5, ConfigManager.Manager.Config.GetIntValue("pean_classd_ammo", 120));
            }
            if(ev.TeamRole.Team == Smod2.API.Team.CHAOS_INSURGENCY)
            {
                ev.Items.Add(ItemType.LOGICER);
                ev.Items.Add(ItemType.CHAOS_INSURGENCY_DEVICE);
                ev.Items.Add(ItemType.RADIO);
                ev.Items.Add(ItemType.FRAG_GRENADE);
                ev.Items.Add(ItemType.WEAPON_MANAGER_TABLET);

                p.SetHealth(ConfigManager.Manager.Config.GetIntValue("pean_ic_hp", 250));
                p.SetAmmo(AmmoType.DROPPED_5, ConfigManager.Manager.Config.GetIntValue("pean_ic_ammo", 200));
            }
        }

        public void OnTeamRespawn(TeamRespawnEvent ev)
        {
            if(ev.SpawnChaos == false)
            {
                ev.SpawnChaos = true;
            }
        }
    }
}